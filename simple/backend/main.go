package main


import (
	"fmt"
	"github.com/julienschmidt/httprouter"
	"io/ioutil"
	"mime"
	"net/http"
	"log"
	"path/filepath"
	"strings"
)

func Serve(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	split := strings.Split(r.URL.Path, "/")
	fmt.Println(split)
	if len (split) < 1 {
		return
	}
	if len(split)==3 {
		split = append (split, split[1]+".html")
	}
	file:="."+strings.Join(split,string(filepath.Separator))
	fmt.Println(file)
	html, _ := ioutil.ReadFile(file)
	w.Header().Add("Content-Type", mime.TypeByExtension(filepath.Ext(file)))
	w.Write(html)

}




func main() {
	router := httprouter.New()
	router.GET("/main/*filepath", Serve)
	router.GET("/menu/*filepath", Serve)
	router.GET("/home/*filepath", Serve)

	log.Fatal(http.ListenAndServe(":8011", router))
}
