package main

import (
        "fmt"
        "github.com/alexflint/go-arg"
        "github.com/gotk3/gotk3/glib"
        "github.com/sourcegraph/go-webkit2/webkit2"
        "io/ioutil"
        "log"
        "os"

        "github.com/gotk3/gotk3/gtk"
)

func setupWebview(uri, uritype string, widht, height int) (w *webkit2.WebView) {
        w = webkit2.NewWebView()
        _, err := w.Connect("load-failed", func() {
                fmt.Println("Load failed.")
        })
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }
        _, err = w.Connect("load-changed", func(_ *glib.Object, i int) {
                loadEvent := webkit2.LoadEvent(i)
                switch loadEvent {
                case webkit2.LoadFinished:
                        fmt.Println("Load finished.")
                        fmt.Printf("URI: %s\n", uri)
                }
        })
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }
        _, err = glib.IdleAdd(func() bool {
                if uritype=="url" {
                        w.LoadURI(uri)
                }
                if uritype=="file" {
                        html, _ := ioutil.ReadFile(uri)
                        w.LoadHTML(string(html), "")
                }
                return false
        })
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }

        err = w.Widget.Set("expand", true)
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }

        if widht + height > 0 {
                w.SetSizeRequest(widht, height)
        }
        return
}

func main() {
        var args struct {
                WindowMode  bool     `arg:"-w" help:"enable window mode"`
        }
        arg.MustParse(&args)

        // Initialize GTK without parsing any command line arguments.
        gtk.Init(nil)

        // Create a new toplevel window, set its title, and connect it to the
        // "destroy" signal to exit the GTK main loop when it is destroyed.
        win, err := gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
        if err != nil {
                log.Fatal("Unable to create window:", err)
        }
        win.SetTitle("main")
        _, err = win.Connect("destroy", func() {
                gtk.MainQuit()
        })
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }

        if !args.WindowMode {
                win.Fullscreen()
                win.SetBorderWidth(0)
                win.SetDecorated(false)
        } else {
                win.SetDefaultSize(1900, 1200)
        }

        //webViewMain := setupWebview("main.html", "file" , 1700, 1200)
        //webViewMenu := setupWebview("menu.html", "file", 100, 1000 )
        //webViewHome := setupWebview("home.html", "file", 100, 50)
        webViewMain := setupWebview("http://localhost:8011/main", "url" , 1700, 1200)
        webViewMenu := setupWebview("http://localhost:8011/menu", "url" , 100, 1000 )
        webViewHome := setupWebview("http://localhost:8011/home", "url", 100, 100)

        // Add the label to the window.
        gridMain, err := gtk.GridNew()
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }

        gridLeft, err := gtk.GridNew()
        if err != nil {
                fmt.Println(err)
                os.Exit(1)
        }

        gridLeft.SetOrientation(gtk.ORIENTATION_HORIZONTAL)
        gridLeft.Add(webViewMenu)
        gridLeft.AttachNextTo(webViewHome, webViewMenu, gtk.POS_TOP, 1, 1)

        gridMain.SetOrientation(gtk.ORIENTATION_VERTICAL)
        gridMain.Add(webViewMain)
        gridMain.AttachNextTo(gridLeft, webViewMain, gtk.POS_LEFT, 1, 1)

        win.Add(gridMain)
        // Recursively show all widgets contained in this window.
        win.ShowAll()
        // Begin executing the GTK main loop.  This blocks until
        // gtk.MainQuit() is run.
        gtk.Main()
}